require 'stripe'

module Colt
  class Subscription
    
    #
    # email        : The email of the customer
    # stripe_token : Token representing the credit card
    #
    # Returns      : Stripe::Customer object
    # 
    def self.create(email, stripe_token, plan_id, description='none')
      Stripe::Customer.create(email:       email, 
                              description: description, 
                              card:        stripe_token, 
                              plan:        plan_id)
    end
    
    #
    # customer_id : Stripe customer to add a new credit card 
    # plan_id     : The plan to subscribe the customer. It is a string like 'gold'.
    #
    # Returns     :  Stripe::Subscription object
    #
    def self.update(customer_id, plan_id)
      customer = Stripe::Customer.retrieve(customer_id)
      customer.update_subscription(plan: plan_id)
    end
    
    #
    # customer_id : Stripe customer to add a new credit card 
    #
    # Returns     : Stripe::Subscription object
    #
    def self.cancel(customer_id)
      customer = Stripe::Customer.retrieve(customer_id) 
      customer.cancel_subscription
    end
  end
  
end