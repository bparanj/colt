require 'stripe'

module Colt

  class SubscriptionPayment
    SUBSCRIPTION_PAYMENT_FAILED = "invoice.payment_failed"
    
    # Retrieving the event from the Stripe API guarantees its authenticity  
    def initialize(id)
      @event = Stripe::Event.retrieve(id)
    end 
    
    def failed?
      @event.type == SUBSCRIPTION_PAYMENT_FAILED
    end
    
    def stripe_customer_token
      @event.data.object.customer
    end
  end
  
end