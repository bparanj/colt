require "stripe"

require "colt/version"
require "colt/subscription"
require "colt/subscription_payment"

module Colt
  STRIPE_API_VERSION = '2015-09-03 (latest)'  
end