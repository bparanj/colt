# Colt

Colt is a micro gem used to subscribe to a given plan using the Stripe API. The plans must already exist in your account. To experiment with the code, run `bin/console` for an interactive prompt.

## Our Sponsor

This project is sponsored by Zepho Inc. If you are interested in learning TDD in Ruby, you can redeem the coupon here: https://www.udemy.com/learn-test-driven-development-in-ruby/?couponCode=svr

## Features:

1. Recurring billing
	- Subscribe to a plan
	- Change plan 
	- Cancel subscription
2. Process Subscription Payment Failures for Webhooks

## Version

Ruby 	   : 2.3.1
Stripe Gem : 1.43

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'colt'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install colt

## Usage

Please read the tests written with readability in mind. They are executable and human comprehensible documentation. Here is a sample Rails app that uses this gem to implement subscription feature: https://bitbucket.org/bparanj/striped

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org). To run the tests:

```ruby
$ruby -rminitest/pride test/colt_integration_test.rb --verbose
$ruby -rminitest/pride test/subscription_test.rb --verbose
$ruby -rminitest/pride test/subscription_payment_test.rb --verbose
```

or 

```ruby
rake
```

to run all the tests.

## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/bparanj/colt.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

