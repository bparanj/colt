require 'minitest/mock'
require_relative 'test_helper'

class SubscriptionTest < Minitest::Test
  include Colt
    
  def test_subscribe_customer_to_a_plan
    email = 'bugs.bunny@rubyplus.com'
    plan_id = 'gold' 
    h = JSON.parse(File.read("test/fixtures/customer.json"))
    c = Stripe::Customer.construct_from(h)      
    
    Stripe::Customer.stub :create, c do
      customer = Subscription.create(email, 'bogus stripe_token', plan_id)
      
      assert_equal 'cus_6XToBT4GczhYXA', customer.id
      assert_equal 'gold', customer.subscriptions.data[0].plan.id
    end

  end
  
  def test_update_subscription_to_a_plan_will_update_the_subscribed_plan
    existing_customer_id = 'cus_6XToBT4GczhYXA' 
    new_plan_id = 'silver' 

    h = JSON.parse(File.read("test/fixtures/customer.json"))
    c = Stripe::Customer.construct_from(h)      
    
    h2 = JSON.parse(File.read("test/fixtures/subscription.json"))
    s = Stripe::Subscription.construct_from(h2)
    
    Stripe::Customer.stub :retrieve, c do
      c.stub :update_subscription, s do
        subscription = Subscription.update(existing_customer_id, new_plan_id)
      
        assert_equal 'silver', subscription.plan.id       
        assert_equal 'active', subscription.status
      end
    end    
  end
  
  def test_cancel_subscription_will_return_subscription_object_with_status_canceled
    existing_customer_id = 'cus_6XTROqQpg2yLCv' 

    h = JSON.parse(File.read("test/fixtures/customer.json"))
    c = Stripe::Customer.construct_from(h)      

    h2 = JSON.parse(File.read("test/fixtures/cancelled_subscription.json"))
    s = Stripe::Subscription.construct_from(h2)
        
    Stripe::Customer.stub :retrieve, c do    
      c.stub :cancel_subscription, s do
        subscription = Subscription.cancel(existing_customer_id)
        
        assert_equal 'canceled', subscription.status  
      end
    end    
  end
  
end