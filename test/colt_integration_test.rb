require_relative 'test_helper'

class ColtIntegrationTest < Minitest::Test
  include Colt
  
  def setup
    Stripe.api_key = ENV['STRIPE_SECRET_KEY']
  end
  
  def test_subscribe_customer_to_gold_plan
    email = 'bugs_bunny@rubyplus.com'
    stripe_token = Stripe::Token.create(card: { 
                                          number: "4242424242424242",
                                          exp_month:  7,
                                          exp_year:  2016,
                                          cvc: "314"
                                          })
    # This plan must already exist in your Stripe Test account
    plan_id = 'gold'
    
    customer = Subscription.create(email, stripe_token.id, plan_id)
    
    # If there is no exception and the response JSON has the new customer id then the test passes
    assert customer.id.size > 5 
  end
  
  def test_update_subscription_to_a_new_plan
    # This must be an existing customer id who already has a subscription for gold plan
    customer_id = 'cus_6Xs62pHajEKLQA'
    new_plan_id = 'silver'
    
    subscription = Subscription.update(customer_id, new_plan_id)
    
    assert_equal new_plan_id, subscription.plan.id
  end
  
  def test_cancel_subscription
    customer_id = 'cus_6Xs62pHajEKLQA'
    plan_id = 'silver'
    subscription = Subscription.update(customer_id, plan_id)
    
    subscription = Subscription.cancel(customer_id)
    
    assert_equal 'canceled', subscription.status
  end
end
