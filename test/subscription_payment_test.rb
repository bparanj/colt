require 'minitest/mock'
require_relative 'test_helper'

class SubscriptionTest < Minitest::Test
  include Colt
  
  def test_subscription_payment_failed_web_hook
    h = JSON.parse(File.read("test/fixtures/subscription_charge_failed.json"))
    e = Stripe::Event.construct_from(h)          
    
    Stripe::Event.stub :retrieve, e do    
      subscription_payment = SubscriptionPayment.new('evt_16flXdKmUHg13gkFbOvSoXn8')
      
      assert subscription_payment.failed?
    end
  end
  
  def test_subscription_payment_failed_web_hook_retrieve_stripe_customer_token
    h = JSON.parse(File.read("test/fixtures/subscription_charge_failed.json"))
    e = Stripe::Event.construct_from(h)          
    
    Stripe::Event.stub :retrieve, e do    
      subscription_payment = SubscriptionPayment.new('evt_16flXdKmUHg13gkFbOvSoXn8')
      
      actual_stripe_customer_token = subscription_payment.stripe_customer_token
      expected_stripe_customer_token = 'cus_6tYeopy1hd0tmZ'
      
      assert_equal actual_stripe_customer_token, expected_stripe_customer_token
    end
    
  end
end